//function to remove errormessages if enter the specific field
function removeAlerts(){
	$('.content').on('focus', '#username', function(){
		$('#no-username-alert').remove();
	});
	
	$('.content').on('focus', '#password', function(){
		$('#no-password-alert').remove();
	});
	
	$('.content').on('focus', '#username', function(){
		$('#no-username-exist-alert').remove();
	});
	
	$('.content').on('focus', '#password', function(){
		$('#wrong-password-alert').remove();
	});
}

$(document).ready(function(){
	
	$.removeCookie('tankstelle_pwd');
	$.removeCookie('tankstelle_username');
	
	$('#register-btn').click(function(){
		window.location.href = "registrieren.html";
	});
	
	$('#login-btn').click(function(){
		$('#no-username-alert').remove();
		$('#no-password-alert').remove();
		$('#no-username-exist-alert').remove();
		$('#wrong-password-alert').remove();
		
		var error = false;
		if($('#username').val() == ""){
			error = true;
			$('#username').after('<div id="no-username-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte einen Benutzername angeben!</div>');
		}
		
		if($('#password').val() == ""){
			error = true;
			$('#password').after('<div id="no-password-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte eine Passwort angeben!</div>');
		}
		
		if(!error){
			$.ajax({
				url : '../php/checkUsername.php?username='+$('#username').val(),
				async:false,
				success : function(result) {
					if(result == "true"){
						$('#username').after('<div id="no-username-exist-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Benutzername nicht vorhanden!</div>');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log("ERROR: login-btn click() - checkUsername");
					console.log(xhr.status);
					console.log(xhr.responseText);
				},
			});
			
			$.ajax({
				url : '../php/checkLogin.php?username='+$('#username').val()+'&pwd='+$.md5($('#password').val()),
				async:false,
				success : function(result) {
					if(result == "false"){
						$('#password').after('<div id="wrong-password-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Falsches Passwort!</div>');
					}else{
						//set cookies and go to mainpage
						$.cookie('tankstelle_pwd',$.md5($('#password').val()));
						$.cookie('tankstelle_username',result);
						window.location.href = "uebersicht.html";
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.status);
					console.log(xhr.responseText);
				},
			});
		}
		
	});
	
	removeAlerts();
	
});