var lat, lon, stationId, stationLat, stationLon;

//load stationdetails
function initializePage(){
	var params = window.location.search;
	stationId = params.substring(params.indexOf('tankstelle=')+11, params.indexOf('&'));
	lat = params.substring(params.indexOf('lat=')+4, params.indexOf('&lon'));
	lon = params.substring(params.indexOf('lon=')+4);
	
	stationLat, stationLon = 0.0;
	var distanceToStation = 0;
	var bleifreiPrice, superPrice, dieselPrice = 0.0;
	
	$.ajax({
		url : '../php/getStationImages.php?id='+stationId,
		async:false,
		dataType: "json",
		success : function(result) {
			var htmlString = '<div class="station-images-wrapper">';
			
			if(result[0][0].length > 0){
				htmlString += '<img class="station-image" src="'+result[0][0]+'"/>'
			}
			if(result[0][1].length > 0){
				htmlString += '<img class="station-image" src="'+result[0][1]+'"/>'
			}
			if(result[0][2].length > 0){
				htmlString += '<img class="station-image" src="'+result[0][2]+'"/>'
			}
			
			htmlString += '</div>';
			$('#navigation-btn').before(htmlString);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		}
	});
	
	//stationDetails
	$.ajax({
		url : '../php/getStation.php?id='+stationId,
		dataType: "json",
		async:false,
		success : function(result) {
			for(var i=0; i<result.length; i++){
				$('.content-wrapper h1').text(result[i][1]);
				
				$('#navigation-btn').before('<div class="row"><div class="col-md-4 col-sm-4 col-xs-4 bold">Adresse:</div><div class="col-md-8 col-sm-8 col-xs-8">'+result[i][2]+'</div></div>');
				
				$('#navigation-btn').before('<div class="row"><div class="col-md-4 col-sm-4 col-xs-4 bold">Preise:</div><div class="col-md-4 col-sm-4 col-xs-4">Bleifrei 95</div><div class="col-md-4 col-sm-4 col-xs-4">'+result[i][5]+' CHF/l</div></div>');
				$('#navigation-btn').before('<div class="row"><div class="col-md-4 col-sm-4 col-xs-4"></div><div class="col-md-4 col-sm-4 col-xs-4">Super</div><div class="col-md-4 col-sm-4 col-xs-4">'+result[i][6]+' CHF/l</div></div>');
				$('#navigation-btn').before('<div class="row"><div class="col-md-4 col-sm-4 col-xs-4"></div><div class="col-md-4 col-sm-4 col-xs-4">Diesel</div><div class="col-md-4 col-sm-4 col-xs-4">'+result[i][7]+' CHF/l</div></div>');
				
				stationLat = result[i][3];
				stationLon = result[i][4];
				
				bleifreiPrice = result[i][5];
				superPrice = result[i][6];
				dieselPrice = result[i][7];
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		}
	});
	
	//calculate distance to station
	var distanceService = new google.maps.DistanceMatrixService();
	distanceService.getDistanceMatrix({
        origins: [new google.maps.LatLng(lat,lon)],
        destinations: [new google.maps.LatLng(stationLat,stationLon)],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        durationInTraffic: true,
        avoidHighways: false,
        avoidTolls: false
    },
    function (response, status) {
        if (status !== google.maps.DistanceMatrixStatus.OK) {
            console.log('Error:', status);
        } else {
            for(var i=0; i<response.rows.length; i++){
            	var results = response.rows[i].elements;
            	for(var j=0; j<results.length; j++){
            		distanceToStation = results[j].distance.value;
            		$('#navigation-btn').before('<div class="row"><div class="col-md-4 col-sm-4 col-xs-4 bold">Anfahrsdauer:</div><div class="col-md-8 col-sm-8 col-xs-8">'+results[j].duration.text+'</div></div>');
    				
            	}
            }
            calculatePicesToStation(distanceToStation,bleifreiPrice,superPrice,dieselPrice)
        }
    });
	
}

function calculatePicesToStation(distanceToStation,bleifreiPrice,superPrice,dieselPrice){
	$.ajax({
		url : '../php/getUsersVehicles.php?username='+$.cookie('tankstelle_username'),
		async:false,
		success : function(result) {
			if(result.length > 0){
				var first = true;
				var resultString = result;
				while(resultString.indexOf(',') != -1){
					var vehicleId = resultString.substring(0,resultString.indexOf(','));
					addDrivingCosts(vehicleId,distanceToStation,bleifreiPrice,superPrice,dieselPrice,first);
					resultString = resultString.substring(resultString.indexOf(',')+1);
					first = false;
				}
				addDrivingCosts(resultString,distanceToStation,bleifreiPrice,superPrice,dieselPrice,first);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

function addDrivingCosts(vehicleId, distance, bleifreiPrice, superPrice, dieselPrice,first){
	$.ajax({
		url : '../php/getVehicleById.php?id='+vehicleId,
		dataType: "json",
		async:false,
		success : function(result) {
			for(var i=0; i<result.length; i++){
				
				var wayCosts = 0.0;
				
				if(result[i][2] == "bleifrei"){
					wayCosts = distance / 1000 * result[i][1] * bleifreiPrice;
				}else if(result[i][2] == "super"){
					wayCosts = distance / 1000 * result[i][1] * superPrice;
				}else if(result[i][2] == "diesel"){
					wayCosts = distance / 1000 * result[i][1] * dieselPrice;
				}
				
				wayCosts = Math.round(wayCosts)/100;
				
				if(first){
					$('#navigation-btn').before('<div class="row"><div class="col-md-4 col-sm-4 col-xs-4 bold">Wegkosten:</div><div class="col-md-4 col-sm-4 col-xs-4">'+result[i][0]+'</div><div class="col-md-4 col-sm-4 col-xs-4">'+wayCosts+' CHF</div></div>');
				}else{
					$('#navigation-btn').before('<div class="row"><div class="col-md-4 col-sm-4 col-xs-4 bold"></div><div class="col-md-4 col-sm-4 col-xs-4">'+result[i][0]+'</div><div class="col-md-4 col-sm-4 col-xs-4">'+wayCosts+' CHF</div></div>');
				}
				
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

//open googlemaps with route to the selected station
function navigateToTarget(){
	var saddr = lat+','+lon;
	var daddr = stationLat+','+stationLon;
	var url = 'https://www.google.com/maps?saddr='+saddr+'&daddr='+daddr;
	
	console.log(url);
	var win = window.open(url, '_blank');
	win.focus();
}

//add admin button
function addAdminButton(){
	$('.header .nav li:nth-child(2)').after('<li role="presentation"><a href="admin.html">Admin</a></li>');
}

$(document).ready(function(){
	
	//check if loged (in cookie)
	if($.cookie('tankstelle_pwd') == undefined || $.cookie('tankstelle_username') == undefined){
		window.location.href = "login.html";
	}else{
		$.ajax({
			url : '../php/checkLogin.php?username='+$.cookie('tankstelle_username')+'&pwd='+$.cookie('tankstelle_pwd'),
			async:false,
			success : function(result) {
				if(result == "false"){
					window.location.href = "login.html";
				}else if(result !=  $.cookie('tankstelle_username')){
					window.location.href = "login.html";
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				window.location.href = "login.html";
			},
		});
	}
	
	//check if admin
	$.ajax({
		url:'../php/isAdmin.php?username='+$.cookie('tankstelle_username'),
		async:false,
		success: function(result){
			if(result == "false"){}else{
				addAdminButton();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {},
	});
	
	$('#settings-btn').click(function(){
		window.location.href = "settings.html";
	});
	
	$('#back-btn').click(function() {
		window.location.href = "uebersicht.html";
	});
	
	$('#navigation-btn').click(function(){
		navigateToTarget();
	});
	
	initializePage();
	
});