
// Load all unproved Tanksellenowner
function loadTankstellenOwner(){
	$.ajax({
		url : '../php/getUnprovedOwners.php',
		aync:false,
		dataType:"json",
		success : function(result) {
			$('.content').append('<h3>Tankstellenbesitzer akzeptieren</h3>');
			for(var i=0;i<result.length;i++){
				$('.content').append('<div class="tankstellen-owner" id="tankstellen-owner-'+result[i][0]+'"><span>'+result[i][1]+'</span><br><button class="proveowner btn btn-success">Accept</button></div>');
			}
			
			$('.proveowner').click(function(){
				setAccept($(this).parent());
			});
			
			addStationChanges();
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

//Set Tankstellenowner as accepted
function setAccept(element){
	var id = element.attr('id').substring(element.attr('id').indexOf('tankstellen-owner-')+18);
	$.ajax({
		url : '../php/setOwnerAccept.php?id='+id,
		aync:false,
		success : function(result) {
			$('#tankstellen-owner-'+id).remove();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

//add all Station with changes
function addStationChanges(){
	$.ajax({
		url : '../php/getChangedStations.php',
		aync:false,
		dataType:"json",
		success : function(result) {
			$('.content').append('<h3>Tankstellenänderungen akzeptieren</h3>');
			for(var i=0;i<result.length;i++){
				
				if(result[i][0] < 0){
					//new Station
					var newstation = '<div class="tankstelle-new" data-id="'+result[i][11]+'" data-ownerid="'+result[i][12]+'"></div>';
					newstation = $(newstation);
					
					newstation.append('<div><span><b>Neue Tankstelle</b></span></div>');
					newstation.append('<div><span>Name:</span><span class="name">'+result[i][1]+'</span></div>');
					newstation.append('<div><span>Adresse:</span><span class="address">'+result[i][2]+'</span></div>');
					newstation.append('<div><span>Koordinaten:</span><span class="cords">'+result[i][4]+'/'+result[i][3]+'</span></div>');
					newstation.append('<div><span>Bleifrei:</span><span class="bleifrei">'+result[i][5]+'</span></div>');
					newstation.append('<div><span>Super:</span><span class="super">'+result[i][6]+'</span></div>');
					newstation.append('<div><span>Diesel:</span><span class="diesel">'+result[i][7]+'</span></div>');
					if(result[i][8] != ""){
						newstation.append('<div><span>Bild1:</span><img class="image1" src="'+result[i][8]+'"></img></div>');
					}
					if(result[i][9] != ""){
						newstation.append('<div><span>Bild2:</span><img class="image2" src="'+result[i][9]+'"></img></div>');
					}
					if(result[i][10] != ""){
						newstation.append('<div><span>Bild3:</span><img class="image3" src="'+result[i][10]+'"></img></div>');
					}
					
					newstation.append('<div><button class="btn btn-success left btn-accept-new-station" type="button">Tankstelle akzeptieren</button><button class="btn btn-danger right btn-not-accept-new-station" type="button">Tankstelle ablehnen</button></div>');
					
					
					$('.content').append(newstation);
				}else{
					if(result[i][2] == ""){
						//delete Station
						var deletestation = '<div class="tankstelle-delete" data-id="'+result[i][11]+'" data-stationid="'+result[i][0]+'"></div>';
						deletestation = $(deletestation);
						
						deletestation.append('<div><span><b>Tankstelle löschen</b></span></div>');
						deletestation.append('<div><span>Name:</span><span>'+result[i][1]+'</span></div>');
						
						deletestation.append('<div><button class="btn btn-danger left btn-accept-delete-station" type="button">Tankstelle löschen</button><button class="btn btn-success right btn-not-delete-station" type="button">Tankstelle nicht löschen</button></div>');
						
						$('.content').append(deletestation);
					}else{
						//change Station
						var changestation = '<div class="tankstelle-change" data-id="'+result[i][11]+'" data-stationid="'+result[i][0]+'"></div>';
						changestation = $(changestation);
						
						changestation.append('<div><span><b>Tankstelle ändern</b></span></div>');
						changestation.append('<div><span>Name:</span><span>'+result[i][1]+'</span></div>');
						
						changestation.append('<div><span>Bleifrei:</span><span class="bleifrei">'+result[i][5]+'</span></div>');
						changestation.append('<div><span>Super:</span><span class="super">'+result[i][6]+'</span></div>');
						changestation.append('<div><span>Diesel:</span><span class="diesel">'+result[i][7]+'</span></div>');
						if(result[i][8] != ""){
							changestation.append('<div><span>Bild1:</span><img class="image1" src="'+result[i][8]+'"></img></div>');
						}
						if(result[i][9] != ""){
							changestation.append('<div><span>Bild2:</span><img class="image2" src="'+result[i][9]+'"></img></div>');
						}
						if(result[i][10] != ""){
							changestation.append('<div><span>Bild3:</span><img class="image3" src="'+result[i][10]+'"></img></div>');
						}
						
						changestation.append('<div><button class="btn btn-success left btn-accept-changes-station" type="button">Änderungen akzeptieren</button><button class="btn btn-danger right btn-not-change-station" type="button">Änderungen verwerfen</button></div>');
						
						
						$('.content').append(changestation);
					}
				}
			}
			
			addStationChangeHandler();
			addChangeCars();
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

//add handler to stationchanges
function addStationChangeHandler(){

	//new station
	$('.btn-accept-new-station').click(function(){
		var wrapper = $(this).parent().parent();
		var ownerid = wrapper.data('ownerid');
		var tempid = wrapper.data('id');
		var cords = wrapper.find('.cords').text();
		var lat = cords.substring(0,cords.indexOf('/'));
		var long =cords.substring(cords.indexOf('/')+1);
		var image1 = (wrapper.find('.image1').length > 0 ? wrapper.find('.image1').attr('src') : "");
		var image2 = (wrapper.find('.image2').length > 0 ? wrapper.find('.image2').attr('src') : "");
		var image3 = (wrapper.find('.image3').length > 0 ? wrapper.find('.image3').attr('src') : "");

		$.ajax({
			url : '../php/addStation.php?name='+wrapper.find('.name').text()+'&address='+wrapper.find('.address').text()+'&long='+long+'&lat='+lat+'&bleifrei='+wrapper.find('.bleifrei').text()+'&diesel='+wrapper.find('.diesel').text()+'&super='+wrapper.find('.super').text()+'&image1='+image1+'&image2='+image2+'&image3='+image3+'&ownerid='+ownerid,
			async:false,
			success : function(result) {
				$.ajax({
					url : '../php/deleteTempStation.php?id='+tempid,
					aync:false,
					success : function(result) {
						wrapper.remove();
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
					},
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
			},
		});
	
	});
	
	$('.btn-not-accept-new-station').click(function(){
		var wrapper = $(this).parent().parent();
		var id = wrapper.data('id');
		$.ajax({
			url : '../php/deleteTempStation.php?id='+id,
			aync:false,
			success : function(result) {
				wrapper.remove();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
			},
		});
	});
	
	//change station
	$('.btn-accept-changes-station').click(function(){
		var wrapper = $(this).parent().parent();
		var tempid = wrapper.data('id');
		var stationId  = wrapper.data('stationid');
		var image1 = (wrapper.find('.image1').length > 0 ? wrapper.find('.image1').attr('src') : "");
		var image2 = (wrapper.find('.image2').length > 0 ? wrapper.find('.image2').attr('src') : "");
		var image3 = (wrapper.find('.image3').length > 0 ? wrapper.find('.image3').attr('src') : "");

		$.ajax({
			url : '../php/changeStation.php?bleifrei='+wrapper.find('.bleifrei').text()+'&diesel='+wrapper.find('.diesel').text()+'&super='+wrapper.find('.super').text()+'&image1='+image1+'&image2='+image2+'&image3='+image3+'&id='+stationId,
			async:false,
			success : function(result) {
				$.ajax({
					url : '../php/deleteTempStation.php?id='+tempid,
					aync:false,
					success : function(result) {
						wrapper.remove();
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
					},
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
			},
		});
	});
	
	$('.btn-not-change-station').click(function(){
		var wrapper = $(this).parent().parent();
		var id = wrapper.data('id');
		$.ajax({
			url : '../php/deleteTempStation.php?id='+id,
			aync:false,
			success : function(result) {
				wrapper.remove();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
			},
		});
	});
	
	//delete station
	$('.btn-accept-delete-station').click(function(){
		var wrapper = $(this).parent().parent();
		var tempid = wrapper.data('id');
		var stationId  = wrapper.data('stationid');

		$.ajax({
			url : '../php/deleteStation.php?id='+stationId,
			async:false,
			success : function(result) {
				$.ajax({
					url : '../php/deleteTempStation.php?id='+tempid,
					aync:false,
					success : function(result) {
						wrapper.remove();
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
					},
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
			},
		});
	});

	$('.btn-not-delete-station').click(function(){
		var wrapper = $(this).parent().parent();
		var id = wrapper.data('id');
		$.ajax({
			url : '../php/deleteTempStation.php?id='+id,
			aync:false,
			success : function(result) {
				wrapper.remove();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
			},
		});
	});
	
}

//add all changed cars
function addChangeCars(){
	$.ajax({
		url : '../php/getChangedVehicles.php',
		aync:false,
		dataType:"json",
		success : function(result) {
			$('.content').append('<h3>Neue Fahrzeuge akzeptieren</h3>');
			for(var i=0;i<result.length;i++){
				
				var newvehicle = '<div class="vehicle-new" data-id="'+result[i][0]+'"></div>';
				newvehicle = $(newvehicle);
					
				newvehicle.append('<div><span><b>Neues Fahrzeug</b></span></div>');
				newvehicle.append('<div><span>Name:</span><span class="name">'+result[i][1]+'</span></div>');
				newvehicle.append('<div><span>Verbrauch:</span><span class="verbrauch">'+result[i][2]+'</span></div>');
				newvehicle.append('<div><span>Kraftstoffart:</span><span class="kraftstoff">'+result[i][4]+'</span></div>');
				if(result[i][8] != ""){
					newvehicle.append('<div><span>Bild:</span><img class="image" src="'+result[i][3]+'"></img></div>');
				}
				
				newvehicle.append('<div><button class="btn btn-success left btn-accept-new-vehicle" type="button">Fahrzeug akzeptieren</button><button class="btn btn-danger right btn-not-accept-new-vehicle" type="button">Fahrzeug ablehnen</button></div>');
							
				$('.content').append(newvehicle);			
			}
			
			addCarChangeHandler();
			
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

function addCarChangeHandler(){
	$('.btn-accept-new-vehicle').click(function(){
		var wrapper = $(this).parent().parent();
		var id = wrapper.data('id');
		var image = (wrapper.find('.image').length > 0 ? wrapper.find('.image').attr('src') : "");
		
		$.ajax({
			url : '../php/addVehicle.php?name='+wrapper.find('.name').text()+'&verbrauch='+wrapper.find('.verbrauch').text()+'&kraftstoff='+wrapper.find('.kraftstoff').text()+'&image='+image,
			async:false,
			success : function(result) {
				$.ajax({
					url : '../php/deleteTempVehicle.php?id='+id,
					aync:false,
					success : function(result) {
						wrapper.remove();
					},
					error: function (xhr, ajaxOptions, thrownError) {
						console.log(xhr.status);
						console.log(xhr.responseText);
					},
				});
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
			},
		});
	});
	
	$('.btn-not-accept-new-vehicle').click(function(){
		var wrapper = $(this).parent().parent();
		var id = wrapper.data('id');
		$.ajax({
			url : '../php/deleteTempVehicle.php?id='+id,
			aync:false,
			success : function(result) {
				wrapper.remove();
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(xhr.responseText);
			},
		});
	});
}

//add button to adminview
function addAdminButton(){
	$('.header .nav li:nth-child(2)').after('<li class="active" role="presentation"><a href="#">Admin</a></li>');
}

$(document).ready(function(){
	
	//check if logged (in cookie)
	if($.cookie('tankstelle_pwd') == undefined || $.cookie('tankstelle_username') == undefined){
		window.location.href = "login.html";
	}else{
		$.ajax({
			url : '../php/checkLogin.php?username='+$.cookie('tankstelle_username')+'&pwd='+$.cookie('tankstelle_pwd'),
			async:false,
			success : function(result) {
				if(result == "false"){
					window.location.href = "login.html";
				}else if(result !=  $.cookie('tankstelle_username')){
					window.location.href = "login.html";
				}else{
					//check if admin
					$.ajax({
						url:'../php/isAdmin.php?username='+$.cookie('tankstelle_username'),
						async:false,
						success: function(result){
							if(result == "false"){
								window.location.href = "login.html";
							}else{
								addAdminButton();
								loadTankstellenOwner();
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {
							window.location.href = "login.html";
						},
					});
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				window.location.href = "login.html";
			},
		});
	}

});