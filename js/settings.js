var availableCars;
var userId;
var lat = 0.0;		//userposition - latitude
var lon = 0.0;		//userposition - longitude

//load all existing cars from database to array
function availableCars(){
    availableCars = [];

    $.ajax({
        url : '../php/getVehicles.php',
        dataType: "json",
        async:false,
        success : function(result) {
            for(var i=0;i<result.length;i++) {
                availableCars.push(result[i]);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });
}

//save new password to database
function changePassword(){
    $('#old-password-alert').remove();
    $('#no-password-alert').remove();
    $('#no-password-repeat-alert').remove();
    $('#password-not-same-alert').remove();

    var withoutError = true;

    var oldpwd = $.md5($('#old-password').val());

    $.ajax({
        url : '../php/checkLogin.php?username='+$.cookie('tankstelle_username')+'&pwd='+oldpwd,
        async:false,
        success : function(result) {
            if(result == "false"){
                withoutError = false;
                $('#old-password').after('<div id="old-password-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Altes Passwort falsch!</div>');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });

    if(withoutError && $('#password').val() == ""){
        withoutError = false;
        $('#password').after('<div id="no-password-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte ein Passwort angeben!</div>');
    }

    if(withoutError && $('#password-repeat').val() == ""){
        withoutError = false;
        $('#password-repeat').after('<div id="no-password-repeat-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte ein Passwort angeben!</div>');
    }

    if(withoutError && $('#password-repeat').val() != $('#password').val()){
        withoutError = false;
        $('#password-repeat').after('<div id="password-not-same-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Passwort und Wiederholung nicht identisch!</div>');
    }

    if(withoutError){
        var pwd = $.md5($('#password').val());
        $.ajax({
            url : '../php/changePWD.php?username='+$.cookie('tankstelle_username')+'&pwd='+pwd,
            async:false,
            success : function(result) {
                $('#password').val('');
                $('#password-repeat').val('');
                $('#old-password').val('');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
            },
        });
    }
}

//load all car of the user
function loadCars(){
    $.ajax({
        url : '../php/getUsersVehicles.php?username='+$.cookie('tankstelle_username'),
        async:false,
        success : function(result) {
            if(result.length > 0){
                var resultString = result;
                while(resultString.indexOf(',') != -1){
                    var vehicleId = resultString.substring(0,resultString.indexOf(','));
                    addCar(vehicleId);
                    resultString = resultString.substring(resultString.indexOf(',')+1);
                }
                addCar(resultString);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });
}

//add the car with the id to the DOM
function addCar(vehicleId){
    $.ajax({
        url : '../php/getVehicleById.php?id='+vehicleId,
        dataType: "json",
        async:false,
        success : function(result) {
            for(var i=0; i<result.length; i++){

                $('#vehicle-0').before('<div id="vehicle-details-'+vehicleId+'" class="vehicle-details"><span class="vehicle-details-name">'+result[i][0]+'</span><span class="vehicle-details-verbrauch">'+result[i][1]+' l/100km</span><span class="vehicle-details-kraftstoff">'+result[i][2]+'</span></div><div id="delete-icon-'+vehicleId+'" class="delete-icon"></div>');

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });

    $.ajax({
        url : '../php/getVehicleImage.php?id='+vehicleId,
        async:false,
        success : function(result) {
            $('#vehicle-details-'+vehicleId).find('.vehicle-details-name').before('<img class="vehicle-image" src="'+result+'"/>');
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });

}

//write changing in cars to database
function saveCars(){
    var carString = "";

    $('.vehicle-details').each(function(index){
        if(carString.length > 0){
            carString += ","+$(this).attr('id').substring(16);
        }else{
            carString = $(this).attr('id').substring(16);
        }
    });

    $('.vehicle-input').each(function(index){
        var name = $(this).val();
        $.ajax({
            url : '../php/getVehicleId.php?name='+name,
            async:false,
            success : function(result) {
                if(carString.length > 0 && result.length > 0){
                    carString += ",";
                }
                carString += result;
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
            },
        });
    });

    $.ajax({
        url : '../php/changeVehicles.php?username='+$.cookie('tankstelle_username')+'&vehicles='+carString,
        async:false,
        success : function(result) {
            $('.vehicle-input').val('');
            location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });
}

//remove errormessage if enter specific field
function removeAlerts(){

    $('.content').on('focus', '#old-password', function(){
        $('#old-password-alert').remove();
    });

    $('.content').on('focus', '#password', function(){
        $('#no-password-alert').remove();
    });

    $('.content').on('focus', '#password-repeat', function(){
        $('#no-password-repeat-alert').remove();
    });

    $('.content').on('focus', '#password-repeat, #password', function(){
        $('#password-not-same-alert').remove();
    });

    $('.content').on('focus', '.vehicle-input', function(){
        $('#no-vehicle-alert').remove();
    });

    $('.content').on('focus', '.stations-wrapper .name', function(){
        $('#no-name-alert').remove();
    });

    $('.content').on('focus', '.stations-wrapper .address', function(){
        $('#no-address-alert').remove();
    });

    $('.content').on('focus', '.stations-wrapper .lat', function(){
        $('#no-lat-alert').remove();
    });

    $('.content').on('focus', '.stations-wrapper .long', function(){
        $('#no-long-alert').remove();
    });

    $('.content').on('focus', '.stations-wrapper .bleifrei', function(){
        $('#no-bleifrei-alert').remove();
    });

    $('.content').on('focus', '.stations-wrapper .super', function(){
        $('#no-super-alert').remove();
    });

    $('.content').on('focus', '.stations-wrapper .diesel', function(){
        $('#no-diesel-alert').remove();
    });
	
	$('.content').on('focus', '.cars-wrapper .name', function(){
        $('#no-vehicle-name-alert').remove();
    });
	
	$('.content').on('focus', '.cars-wrapper .verbrauch', function(){
        $('#no-verbrauch-alert').remove();
    });
	
}

//add all stations from this user to DOM
function addStationSettings(){
    $.ajax({
        url : '../php/isOwner.php?username='+$.cookie('tankstelle_username'),
        async:false,
        success : function(result) {
            if(result != "false"){
                userId = result;
                $('.add-new-car').after('<h1>Tankstellen Verwalten</h1>');
                loadUsersStations(userId);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });
}

function loadUsersStations(userId){
    $.ajax({
        url : '../php/getUsersStations.php?userid='+userId,
        async:false,
        dataType:"json",
        success : function(result) {
            for(var i=0;i<result.length;i++){
                $('.content').append('<div class="stations-wrapper" id="station-'+result[i][0]+'"><span class="station-title">'+result[i][1]+'</span></div>');
                $('#station-'+result[i][0]).append('<span class="stations-label">Bleifrei:</span><input class="bleifrei form-control" value="'+result[i][5]+'">');
                $('#station-'+result[i][0]).append('<span class="stations-label">Super:</span><input class="super form-control" value="'+result[i][6]+'">');
                $('#station-'+result[i][0]).append('<span class="stations-label">Diesel:</span><input class="diesel form-control" value="'+result[i][7]+'">');
                $('#station-'+result[i][0]).append('<span class="stations-label">Bild1:</span><input class="image1 form-control" value="'+result[i][8]+'">');
                $('#station-'+result[i][0]).append('<span class="stations-label">Bild2:</span><input class="image2 form-control" value="'+result[i][9]+'">');
                $('#station-'+result[i][0]).append('<span class="stations-label">Bild3:</span><input class="image3 form-control" value="'+result[i][10]+'">');

                $('#station-'+result[i][0]).append('<span class="hidden-address" style="display:none">'+result[i][2]+'</span>');
                $('#station-'+result[i][0]).append('<span class="hidden-long" style="display:none">'+result[i][3]+'</span>');
                $('#station-'+result[i][0]).append('<span class="hidden-lat" style="display:none">'+result[i][4]+'</span>');

                $('#station-'+result[i][0]).append('<button class="btn btn-success left save-station" type="button">Änderungen speichern</button>');
                $('#station-'+result[i][0]).append('<button class="btn btn-danger right delete-station" type="button">Tankstelle löschen</button>');
            }

            $('.content').append('<button class="btn btn-info center add-station" type="button" style="margin-top:50px">Tankstelle hinzufügen</button>');

            addStationHandler();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });
}

function addStationHandler(){
    $('.content').on('click', '.save-station', function(){
        saveExistStation($(this).parent());
    });

    $('.content').on('click', '.delete-station', function(){
        deleteStation($(this).parent());
    });

    $('.add-station').click(function(){
        var newelement = '<div class="stations-wrapper" id="station--1"><span class="station-title">Neue Tankstelle</span></div>';
        newelement = $(newelement);
        newelement.append('<span class="stations-label">Name:</span><input class="name form-control">');
        newelement.append('<span class="stations-label">Adresse:</span><input class="address form-control">');
        newelement.append('<span class="stations-label">Longitude:</span><input class="lat form-control" disabled>');
        newelement.append('<span class="stations-label">Latitude:</span><input class="long form-control" disabled>');
		newelement.append('<div class="location-selector" id="location-selector"></div>');
        newelement.append('<span class="stations-label">Bleifrei:</span><input class="bleifrei form-control">');
        newelement.append('<span class="stations-label">Super:</span><input class="super form-control">');
        newelement.append('<span class="stations-label">Diesel:</span><input class="diesel form-control">');
        newelement.append('<span class="stations-label">Bild1:</span><input class="image1 form-control">');
        newelement.append('<span class="stations-label">Bild2:</span><input class="image2 form-control">');
        newelement.append('<span class="stations-label">Bild3:</span><input class="image3 form-control">');

        newelement.append('<button class="btn btn-success center save-added-station" type="button">Tankstelle speichern</button>');

        $(this).before(newelement);
		$(this).remove();
		
		initializeMap();
    });

    $('.content').on('click', '.save-added-station', function(){
        var parent = $(this).parent();
        saveAddedStation(parent);
    });

}

//initialize googlemaps to select position of new station
function initializeMap() {
	$('#location-selector').height($('#location-selector').width());
	
	var map;
	
	var myLatlng = new google.maps.LatLng(lat, lon);

	var mapOptions = {
		zoom : 11,
		center : myLatlng
	}

	map = new google.maps.Map(document.getElementById('location-selector'), mapOptions);

	google.maps.event.addListener(map, 'click', function(event) {
	   $('#station--1').find('.lat').val(event.latLng.lng());
	   $('#station--1').find('.long').val(event.latLng.lat());
	});
}

//delete own station
function deleteStation(element){
    var id = element.attr("id").substring(element.attr("id").indexOf('station-')+8);
    $.ajax({
        url : '../php/saveStationsChange.php?id='+id+'&name='+element.find('.station-title').text(),
        async:false,
        success : function(result) {
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });
}

//save changes to a existing station
function saveExistStation(element){
    var id = element.attr("id").substring(element.attr("id").indexOf('station-')+8);
    $.ajax({
        url : '../php/saveStationsChange.php?id='+id+'&name='+element.find('.station-title').text()+'&address='+element.find('.hidden-address').text()+'&long='+element.find('.hidden-long').text()+'&lat='+element.find('.hidden-lat').text()+'&bleifrei='+element.find('.bleifrei').val()+'&diesel='+element.find('.diesel').val()+'&super='+element.find('.super').val()+'&image1='+element.find('.image1').val()+'&image2='+element.find('.image2').val()+'&image3='+element.find('.image3').val()+'&ownerid='+userId,
        async:false,
        success : function(result) {
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });
}

//save a new station to database
function saveAddedStation(element){
    var withoutError = true;

    $('#no-name-alert').remove();
    $('#no-address-alert').remove();
    $('#no-lat-alert').remove();
    $('#no-long-alert').remove();
    $('#no-bleifrei-alert').remove();
    $('#no-diesel-alert').remove();
    $('#no-super-alert').remove();

    if(withoutError && element.find('.name').val() == ""){
        withoutError = false;
        element.find('.name').after('<div id="no-name-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte einen Namen angeben!</div>');
    }

    if(withoutError && element.find('.address').val() == ""){
        withoutError = false;
        element.find('.address').after('<div id="no-address-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte eine Adresse angeben!</div>');
    }

    if(withoutError && element.find('.lat').val() == ""){
        withoutError = false;
        element.find('.lat').after('<div id="no-lat-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte eine Longitude angeben!</div>');
    }

    if(withoutError && element.find('.long').val() == ""){
        withoutError = false;
        element.find('.long').after('<div id="no-long-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte eine Latitude angeben!</div>');
    }

    if(withoutError && element.find('.bleifrei').val() == ""){
        withoutError = false;
        element.find('.bleifrei').after('<div id="no-bleifrei-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte eine Preis für Bleifrei angeben!</div>');
    }

    if(withoutError && element.find('.super').val() == ""){
        withoutError = false;
        element.find('.super').after('<div id="no-super-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte eine Preis für Super angeben!</div>');
    }

    if(withoutError && element.find('.diesel').val() == ""){
        withoutError = false;
        element.find('.diesel').after('<div id="no-diesel-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte eine Preis für Diesel angeben!</div>');
    }

    if(withoutError){
        var id = element.attr("id").substring(element.attr("id").indexOf('station-')+8);
        $.ajax({
            url : '../php/saveStationsChange.php?id='+id+'&name='+element.find('.name').val()+'&address='+element.find('.address').val()+'&long='+element.find('.long').val()+'&lat='+element.find('.lat').val()+'&bleifrei='+element.find('.bleifrei').val()+'&diesel='+element.find('.diesel').val()+'&super='+element.find('.super').val()+'&image1='+element.find('.image1').val()+'&image2='+element.find('.image2').val()+'&image3='+element.find('.image3').val()+'&ownerid='+userId,
            async:false,
            success : function(result) {
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
            },
        });
    }
}

//set userposition
function setPosition(position) {
	lat = position.coords.latitude;
	lon = position.coords.longitude;
}

//add handler for create new car
function addNewCarHandler(){
	$('.add-new-car').click(function(){
		var newelement = '<div class="cars-wrapper" id="car--1"><span class="car-title">Neues Fahrzeug</span></div>';
        newelement = $(newelement);
        newelement.append('<span class="stations-label">Name:</span><input class="name form-control">');
        newelement.append('<span class="stations-label">Verbrauch:</span><input class="verbrauch form-control">');
        newelement.append('<span class="stations-label">Kraftstoff:</span><select class="kraftstoff form-control"><option value="bleifrei">Bleifrei</option><option value="super">Super</option><option value="diesel">Diesel</option></select>');
        newelement.append('<span class="stations-label">Bild:</span><input class="image form-control">');

        newelement.append('<button class="btn btn-success center save-added-vehicle" type="button">Fahrzeug speichern</button>');

        $(this).before(newelement);
		$(this).remove();
		
		$('.save-added-vehicle').click(function(){
			saveAddedVehicle($(this).parent());
		});
		
	});
}

function saveAddedVehicle(element){
	var withoutError = true;

    $('#no-vehicle-name-alert').remove();
	$('#no-verbrauch-alert').remove();

    if(withoutError && element.find('.name').val() == ""){
        withoutError = false;
        element.find('.name').after('<div id="no-vehicle-name-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte einen Namen angeben!</div>');
    }
	
	if(withoutError && element.find('.verbrauch').val() == ""){
        withoutError = false;
        element.find('.verbrauch').after('<div id="no-verbrauch-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte einen Verbrauch angeben!</div>');
    }
	
	if(withoutError){
        $.ajax({
            url : '../php/saveCarsChange.php?name='+element.find('.name').val()+'&verbrauch='+element.find('.verbrauch').val()+'&kraftstoff='+element.find('.kraftstoff').val()+'&image='+element.find('.image').val(),
            async:false,
            success : function(result) {
                location.reload();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(xhr.responseText);
            },
        });
	}

}

//add admin button
function addAdminButton(){
	$('.header .nav li:nth-child(2)').after('<li role="presentation"><a href="admin.html">Admin</a></li>');
}

$(document).ready(function(){

    //check if loged (in cookie)
    if($.cookie('tankstelle_pwd') == undefined || $.cookie('tankstelle_username') == undefined){
        window.location.href = "login.html";
    }else{
        $.ajax({
            url : '../php/checkLogin.php?username='+$.cookie('tankstelle_username')+'&pwd='+$.cookie('tankstelle_pwd'),
            async:false,
            success : function(result) {
                if(result == "false"){
                    window.location.href = "login.html";
                }else if(result !=  $.cookie('tankstelle_username')){
                    window.location.href = "login.html";
                }else{
                    loadCars();
					$('#save-cars-btn').after('<button class="btn btn-success center add-new-car" type="button" style="margin-top:50px">Neues Fahrzeug erstellen</button>');
					addNewCarHandler();
                    addStationSettings();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                window.location.href = "login.html";
            },
        });
    }
	
	//check if admin
	$.ajax({
		url:'../php/isAdmin.php?username='+$.cookie('tankstelle_username'),
		async:false,
		success: function(result){
			if(result == "false"){}else{
				addAdminButton();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {},
	});
	
	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setPosition);
    }

    availableCars();

    $('.vehicle-input').autocomplete({
        source: availableCars
    });

    $('#change-pwd-btn').click(function(){
        changePassword();
    });

    $('#save-cars-btn').click(function(){
        saveCars();
    });

    $('#back-btn').click(function() {
        window.location.href = "uebersicht.html";
    });

    $('.content').on('click', '.delete-icon', function(){
        var id = $(this).attr('id').substring(12);
        $(this).remove();
        $('#vehicle-details-'+id).remove();
    });

    removeAlerts();

    $('#add-more-vehicle-btn').click(function(){
        if($(this).prev().val() == ""){
            if($('#no-vehicle-alert').length == 0){
                $(this).after('<div id="no-vehicle-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte erst ein Fahrzeug erfassen!</div>');
            }
        }else{
            var index = parseInt($(this).prev().attr('id').substring($(this).prev().attr('id').indexOf('-')+1));
            $(this).before('<input id="vehicle-'+(index+1)+'" class="vehicle-input form-control" style="width:90%;" type="text" placeholder="Fahrzeug">');
            $('#vehicle-'+(index+1)).autocomplete({
                source: availableCars
            });
        }
    });

});