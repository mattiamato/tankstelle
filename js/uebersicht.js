var lat = 0.0;		//position latitude of user
var lon = 0.0;		//position longitude of user
var map;

//initialize map with googlemaps
function initializeMap() {
	$('#map-canvas').height($('#map-canvas').width());
	
	console.log(lat + "," + lon);
	var myLatlng = new google.maps.LatLng(lat, lon);

	var mapOptions = {
		zoom : 11,
		center : myLatlng
	}

	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	var marker = new google.maps.Marker({
		position : myLatlng,
		map : map,
		title : 'Meine Position'
	});
	
	addStationsToMap();
}

//set position of user and after initialize map
function setPosition(position) {
	lat = position.coords.latitude;
	lon = position.coords.longitude;
	initializeMap();
}

//add all stations to map
function addStationsToMap() {
	var newimage = {
		url : '../images/icons/station-icon-new.png',
		size : new google.maps.Size(32, 47)
	}
	
	$.ajax({
		url : '../php/getStations.php',
		dataType: "json",
		async:false,
		success : function(result) {
			for(var i=0; i<result.length; i++){
				
				var markerPos = new google.maps.LatLng(result[i][3], result[i][4]);
				var marker = new google.maps.Marker({
					position: markerPos,
					map: map,
					icon: newimage,
					url: 'tankstelledetail.html?tankstelle='+result[i][0]+'&lat='+lat+'&lon='+lon
				});
				
				google.maps.event.addListener(marker, 'click', function() {
				    window.location.href = this.url;
				});
				
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

//load all user vehicles
function loadVehicles(){	
	$.ajax({
		url : '../php/getUsersVehicles.php?username='+$.cookie('tankstelle_username'),
		async:false,
		success : function(result) {
			if(result.length > 0){
				var resultString = result;
				while(resultString.indexOf(',') != -1){
					var vehicleId = resultString.substring(0,resultString.indexOf(','));
					vehicleToSelect(vehicleId);
					resultString = resultString.substring(resultString.indexOf(',')+1);
				}
				vehicleToSelect(resultString);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});	
}

//add all uservehicles to the select element
function vehicleToSelect(vehicleId){
	$.ajax({
		url : '../php/getVehicleById.php?id='+vehicleId,
		dataType: "json",
		async:false,
		success : function(result) {
			for(var i=0; i<result.length; i++){
				$('#vehicle-select').append('<option value="'+vehicleId+'">'+result[i][0]+'</option>');
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

//take selected vehicle and take this data from database and start calculate cheapest station
function calculateCheapest(){
	$('#no-station-found-alert').remove();
	$.ajax({
		url : '../php/getVehicleById.php?id='+$('#vehicle-select').val(),
		dataType: "json",
		async:false,
		success : function(result) {
			for(var i=0; i<result.length; i++){
				var verbrauch = result[i][1];
				var kraftstoff = result[i][2];
				
				console.log(verbrauch+" "+kraftstoff);			
				var cheapest;
				getCheapest(verbrauch, kraftstoff, function(result){
					cheapest = result;
					window.location.href = 'tankstelledetail.html?tankstelle='+cheapest+'&lat='+lat+'&lon='+lon
				});
				
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
		},
	});
}

//return cheapest station
function getCheapest(verbrauch, kraftstoff, callback){
	var radiusUmgebung = 0.4;		//ca 27km
	var tankmenge = 40;
	var cheapest = "";
	var cheapestValue = 99999999;
	
	var minLat = lat - radiusUmgebung;
	var maxLat = lat + radiusUmgebung;
	var minLon = lon - radiusUmgebung;
	var maxLon = lon + radiusUmgebung;
	
	$.ajax({
		url : '../php/getStationsAround.php?minLat='+minLat+'&maxLat='+maxLat+'&minLon='+minLon+'&maxLon='+maxLon,
		dataType: "json",
		async:false,
		success : function(result) {
			for(var i=0; i<result.length; i++){
				
				var value;
				var last = false;
				var price;
				
				if(kraftstoff == 'bleifrei'){
					price = result[i][5];
				}else if(kraftstoff == 'diesel'){
					price = result[i][7];
				}else{
					price = result[i][6];
				}	
				
				if(i == (result.length-1)){
					last = true;
				}
				
				getDistanceToStation(result[i][3],result[i][4], price, result[i][0], last, function(result, price, stationId, last){
					var distanceToStation = result;
					
					value = tankmenge*price + distanceToStation/1000*price;
					
					if(value < cheapestValue){
						cheapestValue = value;
						cheapest = stationId;
					}
					
					if(last){
						callback(cheapest);
					}
				});
			}
			if(result.length == 0){
				noCheapestFound();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
			console.log(xhr.responseText);
			noCheapestFound();
		},
	});
}

function noCheapestFound(){
	$('#find-cheapest-btn').parent().after('<div id="no-station-found-alert" class="alert alert-danger" role="alert"><strong>Fehler!</strong> Keine Tankstelle in ihrer Umgebung gefunden!</div>');
}

//calculate distance to station
function getDistanceToStation(stationLat,stationLon, price, stationId, last, callback){
	//calculate distance to station
	var distanceService = new google.maps.DistanceMatrixService();
	distanceService.getDistanceMatrix({
        origins: [new google.maps.LatLng(lat,lon)],
        destinations: [new google.maps.LatLng(stationLat,stationLon)],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.METRIC,
        durationInTraffic: true,
        avoidHighways: false,
        avoidTolls: false
    },
    function (response, status) {
        if (status !== google.maps.DistanceMatrixStatus.OK) {
            console.log('Error:', status);
        } else {
            for(var i=0; i<response.rows.length; i++){
            	var results = response.rows[i].elements;
            	for(var j=0; j<results.length; j++){
            		callback(results[j].distance.value, price, stationId, last);
            	}
            }
        }
    });
}

//add admin button 
function addAdminButton(){
	$('.header .nav li:nth-child(2)').after('<li role="presentation"><a href="admin.html">Admin</a></li>');
}

$(document).ready(function(){
	
	//check if loged (in cookie)
	if($.cookie('tankstelle_pwd') == undefined || $.cookie('tankstelle_username') == undefined){
		window.location.href = "login.html";
	}else{
		$.ajax({
			url : '../php/checkLogin.php?username='+$.cookie('tankstelle_username')+'&pwd='+$.cookie('tankstelle_pwd'),
			async:false,
			success : function(result) {
				if(result == "false"){
					window.location.href = "login.html";
				}else if(result !=  $.cookie('tankstelle_username')){
					window.location.href = "login.html";
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				window.location.href = "login.html";
			},
		});
	}
	
	//check if admin
	$.ajax({
		url:'../php/isAdmin.php?username='+$.cookie('tankstelle_username'),
		async:false,
		success: function(result){
			if(result == "false"){}else{
				addAdminButton();
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {},
	});
	
	loadVehicles();
	
	$('#settings-btn').click(function(){
		window.location.href = "settings.html";
	});
	
	//get position of the user
	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(setPosition);
    }
	
	$('#find-cheapest-btn').click(function(){
		calculateCheapest();
	});
	
});