var availableCars;

//make array with all existing cars
function availableCars(){
    availableCars = [];

    $.ajax({
        url : '../php/getVehicles.php',
        dataType: "json",
        async:false,
        success : function(result) {
            for(var i=0;i<result.length;i++) {
                availableCars.push(result[i]);
            }

            $('.vehicle-input').autocomplete({
                source: availableCars
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("ERROR: availableCars()");
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });
}

//check data from new user and write to database
function registerUser(){
    $('#no-vehicle-alert').remove();
    $('#no-username-alert').remove();
	$('#no-vname-alert').remove();
	$('#no-nname-alert').remove();
	$('#no-email-alert').remove();
    $('#no-username-exist-alert').remove();
    $('#no-password-alert').remove();
    $('#no-password-repeat-alert').remove();
    $('#password-not-same-alert').remove();

    var error = false;

    // Check Inputs
    if($('#username').val() == ""){
        error = true;
        $('#username').after('<div id="no-username-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte einen Benutzername angeben!</div>');
    }
	
	if($('#vname').val() == ""){
        error = true;
        $('#vname').after('<div id="no-vname-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte einen Vorname angeben!</div>');
    }
	
	if($('#nname').val() == ""){
        error = true;
        $('#nname').after('<div id="no-nname-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte einen Nachname angeben!</div>');
    }
	
	if($('#email').val() == "" || $('#email').val().indexOf('@') == -1){
        error = true;
		$('#email').val('')
        $('#email').after('<div id="no-email-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte eine Emailadresse angeben!</div>');
    }

    if($('#password').val() == ""){
        error = true;
        $('#password').after('<div id="no-password-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte ein Passwort angeben!</div>');
    }

    if($('#password-repeat').val() == ""){
        error = true;
        $('#password-repeat').after('<div id="no-password-repeat-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte ein Passwort angeben!</div>');
    }

    if($('#password-repeat').val() != $('#password').val()){
        error = true;
        $('#password-repeat').after('<div id="password-not-same-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Passwort und Wiederholung nicht identisch!</div>');
    }

    $.ajax({
        url : '../php/checkUsername.php?username='+$('#username').val(),
        async:false,
        success : function(result) {
            if(result == "false"){
                error = true;
                $('#username').after('<div id="no-username-exist-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Benutzername wird schon verwendet!</div>');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("ERROR: registerUser() - checkUsername");
            console.log(xhr.status);
            console.log(xhr.responseText);
        },
    });

    if(!error){
        var pwd = $.md5($('#password').val());
        var carString = "";
        var owner = "false";

        if($('#tankstellenowner-input').is(':checked')){
            owner = "true";
        }

        $('.vehicle-input').each(function(index){
            var name = $(this).val();
            $.ajax({
                url : '../php/getVehicleId.php?name='+name,
                async:false,
                success : function(result) {
                    if(index > 0  && result.length > 0){
                        carString += ",";
                    }
                    carString += result;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log("ERROR: registerUser() - registerUser");
                    console.log(xhr.status);
                    console.log(xhr.responseText);
                },
            });
        });

        $.ajax({
            url : '../php/registerUser.php?username='+$('#username').val()+'&pwd='+pwd+'&cars='+carString+'&owner='+owner+'&vname='+$('#vname').val()+'&nname='+$('#nname').val()+'&email='+$('#email').val()+'&tel='+$('#tel').val(),
            async:false,
            success : function(result) {
                $.cookie('tankstelle_pwd',pwd);
                $.cookie('tankstelle_username',result);
                window.location.href = "uebersicht.html";
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log("ERROR: registerUser() - registerUser");
                console.log(xhr.status);
                console.log(xhr.responseText);
            },
        });
    }
}

//remove errormessage if enter specific field
function removeAlerts(){
    $('.content').on('focus', '.vehicle-input', function(){
        $('#no-vehicle-alert').remove();
    });

    $('.content').on('focus', '#username', function(){
        $('#no-username-alert').remove();
    });
	
	$('.content').on('focus', '#vname', function(){
        $('#no-vname-alert').remove();
    });
	
	$('.content').on('focus', '#nname', function(){
        $('#no-nname-alert').remove();
    });
	$('.content').on('focus', '#email', function(){
        $('#no-email-alert').remove();
    });

    $('.content').on('focus', '#username', function(){
        $('#no-username-exist-alert').remove();
    });

    $('.content').on('focus', '#password', function(){
        $('#no-password-alert').remove();
    });

    $('.content').on('focus', '#password-repeat', function(){
        $('#no-password-repeat-alert').remove();
    });

    $('.content').on('focus', '#password, #password-repeat', function(){
        $('#password-not-same-alert').remove();
    });
}

$(document).ready(function(){
    $.removeCookie('tankstelle_pwd');
    $.removeCookie('tankstelle_username');

	//load cars from database
    availableCars();

    $('#register-btn').click(function(){
        registerUser();
    });

    removeAlerts();

    $('#add-more-vehicle-btn').click(function(){
        if($(this).prev().val() == ""){
            if($('#no-vehicle-alert').length == 0){
                $(this).after('<div id="no-vehicle-alert" class="alert alert-danger" role="alert"><strong>Warnung!</strong> Bitte erst ein Fahrzeug erfassen!</div>');
            }
        }else{
            var index = parseInt($(this).prev().attr('id').substring($(this).prev().attr('id').indexOf('-')+1));
            $(this).before('<input id="vehicle-'+(index+1)+'" class="vehicle-input form-control" style="width:90%;" type="text" placeholder="Fahrzeug">');
            $('#vehicle-'+(index+1)).autocomplete({
                source: availableCars
            });
        }
    });
});